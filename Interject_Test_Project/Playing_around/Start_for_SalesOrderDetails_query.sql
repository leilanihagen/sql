/* Select (SalesOrderID,RevisionNumber,OrderDate,OrderStatusText,SalesOrderNumber,
   SubTotal,TaxAmt,Freight,TotalDue,OrderQtyText,UnitPrice,UnitPriceDiscount,
   LineTotal,ShipToCity,ShipToState,ShipToZip,ShipToCountryCode,ShipToCountryName,
   ModelName,CategoryName) */

/* select SalesOrderID,RevisionNumber,OrderDate,OrderStatusText,SalesOrderNumber,
SubTotal,TaxAmt,Freight,TotalDue,OrderQtyText,UnitPrice,UnitPriceDiscount,
LineTotal,ShipToCity,ShipToState,ShipToZip,ShipToCountryCode,ShipToCountryName,
ModelName,CategoryName 
from AdventureWorks.SalesLT.SalesOrderDetail, AdventureWorks.SalesLT.SalesOrderHeader */

/*
select City c
from AdventureWorks.SalesLT.Address, AdventureWorks.SalesLT.Customer, AdventureWorks.SalesLT.CustomerAddress
where CustomerAddress.CustomerID = Customer.CustomerID and CustomerAddress.AddressID = Address.AddressID;
*/

use AdventureWorks

select distinct SalesOrderDetail.SalesOrderID, RevisionNumber, OrderDate, Status as OrderStatusText,
SalesOrderNumber, SubTotal, TaxAmt, Freight, TotalDue, OrderQty, UnitPrice, UnitPriceDiscount,
LineTotal, c as ShipToCity
from SalesLT.SalesOrderHeader, SalesLT.SalesOrderDetail,
	SalesLT.CustomerAddress, SalesLT.Address, SalesLT.Customer -- For shipping info.
	SalesLT.Product, SalesLT.ProductModel, SalesLT.ProductCategory -- For model and category info.
where SalesOrderHeader.CustomerID = Customer.CustomerID; -- we need to match customers with orders...

