SELECT
	soh.SalesOrderID,
	soh.RevisionNumber,
	soh.OrderDate,
	CASE soh.Status
		WHEN 1 THEN 'In process'
		WHEN 2 THEN 'Approved'
		WHEN 3 THEN 'Backordered'
		WHEN 4 THEN 'Rejected'
		WHEN 5 THEN 'Shipped'
		WHEN 6 THEN 'Cancelled'
	END
	AS OrderStatusText, -- Word description of the Status code.
	soh.SalesOrderNumber,
	soh.SubTotal,
	soh.TaxAmt,
	soh.Freight,
	soh.TotalDue
	--RIGHT((CONCAT('0000', sod.OrderQty)), 4) AS OrderQtyText, -- Zero-padded OrderQty.
	--sod.UnitPrice,
	--sod.UnitPriceDiscount,
	--sod.LineTotal
FROM
	Sales.SalesOrderHeader soh;