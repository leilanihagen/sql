--USE AdventureWorks2014

/*
  Stored procedure to select the records within specified start
  and end dates.
  
  REFERENCE MATERIAL:
  -How to write stored procedures:
   https://www.mssqltips.com/sqlservertutorial/162/how-to-create-a-sql-server-stored-procedure-with-parameters/
   
   QUESTIONS:
   -Should this also be top 100?
   -Should I use DATE or DATETIME?
   */

CREATE OR ALTER PROC [dbo].[SalesOrderDetails]
@StartDate DATETIME,
@EndDate DATETIME
AS
SELECT
		soh.SalesOrderID,
		soh.RevisionNumber,
		soh.OrderDate,
		/*CASE soh.Status
			WHEN 1 THEN 'In process'
			WHEN 2 THEN 'Approved'
			WHEN 3 THEN 'Backordered'
			WHEN 4 THEN 'Rejected'
			WHEN 5 THEN 'Shipped'
			WHEN 6 THEN 'Cancelled'
		END AS OrderStatusText, */
		dbo.ufnGetSalesOrderStatusText(soh.Status) AS OrderStatusText,
		soh.SalesOrderNumber,
		soh.SubTotal,
		soh.TaxAmt,
		soh.Freight,
		soh.TotalDue,
		RIGHT(CONCAT('0000', sod.OrderQty), 4) AS OrderQtyText,
		sod.UnitPrice,
		sod.UnitPriceDiscount,
		sod.LineTotal,
		a.City AS ShipToCity,
		sp.StateProvinceCode AS ShipToState,
		a.PostalCode AS ShipToZip,
		cr.CountryRegionCode AS ShipToCountryCode,
		cr.Name AS ShipToCountryName,
		pm.Name AS ProductModel,
		pc.Name AS CategoryName
	FROM Sales.SalesOrderHeader soh
	/* Join the two most fundamental tables, soh and sod: */
		INNER JOIN Sales.SalesOrderDetail sod
			ON soh.SalesOrderID = sod.SalesOrderID
	/* Join in necessary tables for shipping/location related records: */
		INNER JOIN Person.Address a
			ON soh.ShipToAddressID = a.AddressID
		INNER JOIN Person.StateProvince sp
			ON a.StateProvinceID = sp.StateProvinceID
		INNER JOIN Person.CountryRegion cr
			ON sp.CountryRegionCode = cr.CountryRegionCode
	/* Join in tables necessary for ProductModel record: */
		INNER JOIN Production.Product p
			ON sod.ProductID = p.ProductID
		INNER JOIN Production.ProductModel pm
			ON p.ProductModelID = pm.ProductModelID
	/* Join in tables necessary for CategoryName record: */
		INNER JOIN Production.ProductSubcategory ps
			ON p.ProductSubcategoryID = ps.ProductSubcategoryID
		INNER JOIN Production.ProductCategory pc
			ON ps.ProductCategoryID = pc.ProductCategoryID
	WHERE OrderDate BETWEEN @StartDate and @EndDate
	ORDER BY soh.SalesOrderNumber;