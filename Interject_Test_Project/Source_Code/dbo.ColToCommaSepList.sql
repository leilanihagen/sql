/*
  AUTHOR: Leilani Hagen
  DATE: 11/23/2018
  
  ***NOT WORKING
 
  This SQL will demonstrate shredding XML data into table columns while preserving original data type,
  as well as creating XML back from a table using FOR XML.

  REFERENCE MATERIAL:
  -Stack Overflow post explaining how to use OUTPUT parameters when calling stored procedures from
   inside other stored procedures:
   https://stackoverflow.com/questions/49527124/execute-stored-procedure-inside-another-stored-procedure
  -Article on stored procedures with output parameters:
  https://www.codeproject.com/Articles/794765/SQL-Server-How-to-Write-Stored-Procedures-With-Out
  -Stack Overflow on stored procedures with output parameters:
  https://stackoverflow.com/questions/1589466/execute-stored-procedure-with-an-output-parameter
   
   QUESTIONS:
   
   PROBLEMS:

*/

CREATE OR ALTER PROC [dbo].[ColToCommaSepList]
@tablename NVARCHAR(50)
,@columnname NVARCHAR(50)
,@outputXML XML OUTPUT
AS
	DECLARE @SelectStuff NVARCHAR(500)
	SET @SelectStuff = '
	SELECT
		STUFF( (SELECT ", "' + @columnname
				+ 'FROM' + @tablename
				+ 'FOR XML PATH('')), 1, 1, '')'
	EXEC (@SelectStuff)
	--SET @outputXML = 
	
*
USE AdventureWorks2014

DECLARE @outputXML XML
EXEC [dbo].[ColToCommaSepList]
@tablename = '#BreakfastMenu'
,@columnname = 'Name'
,@outputXML OUTPUT

SELECT @outputXML
*/