/*
  AUTHOR: Leilani Hagen
  DATE: 11/25/2018
 
  In this stored procedure we are creating a method of verifying the accuracy of the software that
  populated the Sales.SalesOrderHeader and Sales.SalesOrderDetail tables. We are doing so by manually
  computing the subtotal (CalculatedSubTotalFromDetail) for each order (based on SalesOrderID), and
  comparing this calculated subtotal to the original subtotal reported in the SalesOrderHeader table
  (OriginalSubTotal).
   
*/

/* RUN THE PROC:

USE AdventureWorks2014

EXEC [dbo].[SalesOrderDataReview]

*/

CREATE OR ALTER PROC [dbo].[SalesOrderDataReview]
AS
	CREATE TABLE #SubTotalPerOrder
	(
		SalesOrderID INT
		,CalculatedSubTotalFromDetail MONEY -- Gets more accurate result from SUM(LineTotal) than NUMERIC type.
	)
	
	INSERT INTO #SubTotalPerOrder
		SELECT
		  /* Calculate the subtotal per order by aggregating LineTotal by SalesOrderID: */
			SalesOrderID
			,SUM(LineTotal) AS CalculatedSubTotalFromDetail
		FROM Sales.SalesOrderDetail
		GROUP BY SalesOrderID

	SELECT
		soh.SalesOrderID
		,soh.SalesOrderNumber
	  /* Compare original subtotal (just a column from SalesOrderHeader) to subtotal calculated above^: */
		,soh.SubTotal AS OriginalSubTotal
		,stpo.CalculatedSubtotalFromDetail
		,(soh.SubTotal - stpo.CalculatedSubTotalFromDetail) AS Difference
	FROM Sales.SalesOrderHeader soh
		INNER JOIN #SubTotalPerOrder stpo
			ON soh.SalesOrderID = stpo.SalesOrderID

-- Things I tried...:

/*
CREATE OR ALTER PROC [dbo].[test]
AS
	SELECT
		soh.SalesOrderID
		,soh.SalesOrderNumber
		,soh.SubTotal AS OriginalSubTotal
	FROM
		(SELECT
			SalesOrderID
			,SUM(LineTotal) AS CalculatedSubTotalFromDetail
		FROM Sales.SalesOrderDetail
		GROUP BY SalesOrderID
		) spo --Alias: SumPerOrder
		INNER JOIN Sales.SalesOrderHeader soh
			ON spo.SalesOrderID = soh.SalesOrderID
*/