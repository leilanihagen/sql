/*
  AUTHOR: Leilani Hagen
  DATE: 10/14/2018

  Stored procedure to select the records within specified start
  and end dates.
  
  REFERENCE MATERIAL:
  -How to write stored procedures:
   https://www.mssqltips.com/sqlservertutorial/162/how-to-create-a-sql-server-stored-procedure-with-parameters/
   
   QUESTIONS:
   -Should I use DATE or DATETIME?
*/

/* 	RUN THE PROC:

USE AdventureWorks2014

EXEC dbo.SalesOrderDetails
@StartDate = '2011-05-01',
@EndDate = '2020-06-28';

 */

CREATE OR ALTER PROC [dbo].[SalesOrderDetails]
@StartDate DATE,
@EndDate DATE
AS
SELECT
		soh.SalesOrderID,
		soh.RevisionNumber,
		soh.OrderDate,
		/*CASE soh.Status
			WHEN 1 THEN 'In process'
			WHEN 2 THEN 'Approved'
			WHEN 3 THEN 'Backordered'
			WHEN 4 THEN 'Rejected'
			WHEN 5 THEN 'Shipped'
			WHEN 6 THEN 'Cancelled'
		END AS OrderStatusText, */
		dbo.ufnGetSalesOrderStatusText(soh.Status) AS OrderStatusText, -- Cleaner way to get Status descriptions.
		soh.SalesOrderNumber,
		soh.SubTotal,
		soh.TaxAmt,
		soh.Freight,
		soh.TotalDue,
    /* Zero-pad the output, with field width of 4, and print as a string "OrderQtyText":
    	 RIGHT() extracts 4 chars from the string input, starting from the right, where the string being extracted
    	 is '0000' + OrderQty number. */
		RIGHT(CONCAT('0000', sod.OrderQty), 4) AS OrderQtyText,
		sod.UnitPrice,
		sod.UnitPriceDiscount,
		sod.LineTotal,
		a.City AS ShipToCity,
		sp.StateProvinceCode AS ShipToState,
		a.PostalCode AS ShipToZip,
		cr.CountryRegionCode AS ShipToCountryCode,
		cr.Name AS ShipToCountryName,
		pm.Name AS ProductModel,
		pc.Name AS CategoryName
	FROM Sales.SalesOrderHeader soh
	/* Join the two most fundamental tables, soh and sod: */
		INNER JOIN Sales.SalesOrderDetail sod
			ON soh.SalesOrderID = sod.SalesOrderID
	/* Join in necessary tables for shipping/location related records: */
		INNER JOIN Person.Address a
			ON soh.ShipToAddressID = a.AddressID
		INNER JOIN Person.StateProvince sp
			ON a.StateProvinceID = sp.StateProvinceID
		INNER JOIN Person.CountryRegion cr
			ON sp.CountryRegionCode = cr.CountryRegionCode
	/* Join in tables necessary for ProductModel record: */
		INNER JOIN Production.Product p
			ON sod.ProductID = p.ProductID
		INNER JOIN Production.ProductModel pm
			ON p.ProductModelID = pm.ProductModelID
	/* Join in tables necessary for CategoryName record: */
		INNER JOIN Production.ProductSubcategory ps
			ON p.ProductSubcategoryID = ps.ProductSubcategoryID
		INNER JOIN Production.ProductCategory pc
			ON ps.ProductCategoryID = pc.ProductCategoryID
	WHERE OrderDate BETWEEN @StartDate AND @EndDate
	ORDER BY soh.SalesOrderNumber;