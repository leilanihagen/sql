/*
  AUTHOR: Leilani Hagen
  DATE: 11/20/2018
 
  This SQL will demonstrate an alternate approach to dbo.SalesOrderDataReview, this time using a
  CTE in the query.

  REFERENCE MATERIAL:
  -Article explaining basic usage of CTEs:
  https://www.essentialsql.com/introduction-common-table-expressions-ctes/
  -Article explaining aggregation and GROUP BY:
  https://www.codeproject.com/Articles/1110163/SQL-GROUP-By-and-the-Column-name-is-invalid-in-the
  
   
   QUESTIONS:
   
   
   PROBLEMS:
   
*/

CREATE OR ALTER PROC [dbo].[SalesOrderDataReviewCTE]
AS
	WITH SumPerOrder (SalesOrderID, CalculatedSubTotalFromDetail)
	AS
	(
	SELECT
		SalesOrderID
		,SUM(LineTotal)
	FROM AdventureWorks2014.Sales.SalesOrderDetail
	GROUP BY SalesOrderID
	)
	SELECT
		soh.SalesOrderID
		,soh.SalesOrderNumber
		,soh.SubTotal AS OriginalSubTotal
		,spo.CalculatedSubTotalFromDetail
		,(soh.SubTotal - spo.CalculatedSubTotalFromDetail) AS Difference
	FROM AdventureWorks2014.Sales.SalesOrderHeader soh
		INNER JOIN SumPerOrder spo
			ON soh.SalesOrderID = spo.SalesOrderID
	ORDER BY soh.SalesOrderID

/* Code to run it:

USE AdventureWorks2014

EXEC [dbo].[SalesOrderDataReviewCTE]
			
*/
			
/*	Things I tried...
CREATE OR ALTER PROC [dbo].[SalesOrderDataReviewCTE]
AS
	WITH
	SumPerOrder (SalesOrderID, CalculatedSubTotalFromDetail)
		AS
		(
		SELECT
			SalesOrderID
			,SUM(LineTotal) AS CalculatedSubTotalFromDetail
		FROM Sales.SalesOrderDetail
		GROUP BY SalesOrderID
		),
	OrderInfo (SalesOrderID, SalesOrderNumber, OriginalSubTotal)
		AS
		(
		SELECT
			SalesOrderID
			,SalesOrderNumber
			,SubTotal AS OriginalSubTotal
		FROM Sales.SalesOrderHeader
		)
	SELECT
		o.SalesOrderID
		,SalesOrderNumber
		,OriginalSubTotal
		,CalculatedSubTotalFromDetail
		,(OriginalSubTotal - CalculatedSubTotalFromDetail) AS Difference
	FROM SumPerOrder s
		INNER JOIN OrderInfo o
			ON s.SalesOrderID = o.SalesOrderID
*/		