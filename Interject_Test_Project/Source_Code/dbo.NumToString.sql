--USE AdventureWorks2014

/*
  AUTHOR: Leilani Hagen
  DATE: 12/12/2018

  Stored proc that takes @Number and converts it into a string of length @StringSize, padded with zeros for any
  additional space and truncated to fit @StringSize if too big.

  REFERENCE MATERIAL:
  -Microsoft doc on using output parameters with stored procs:
  https://docs.microsoft.com/en-us/sql/connect/jdbc/using-a-stored-procedure-with-output-parameters?view=sql-server-2017

*/

/* RUN THE PROC:

EXEC [dbo].[NumToString]
@Number = 7
,@StringSize = 3

*/

CREATE OR ALTER PROC [dbo].[NumToString]
@Number INT
,@StringSize INT
AS

  /* Create a string we can use to pad @Number with zeros: */
  DECLARE @NumString NVARCHAR(MAX)
  SET @NumString = '0' + @Number

  /* Prefix our @Number with a 0 for each iteration of the loop: */
  WHILE (LEN(@NumString) < @StringSize) -- Make sure we don't add zeros to a number that's already too long.
  BEGIN
  /* Add a zero on each iteration: */
    SET @NumString = '0' + @NumString
  END

  /* If the original @Number from the user was too long to begin with, truncate it: */
  IF (LEN(@NumString) > @StringSize)
  BEGIN
    SET @NumString = RIGHT(CAST(@Number AS NVARCHAR), @StringSize) -- Put our @Number (too long) into a string and truncate it
  END                                                       -- saving numbers from least significant place value to most.

  SELECT @NumString
