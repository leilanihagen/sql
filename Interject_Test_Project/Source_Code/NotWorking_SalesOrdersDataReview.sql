/*
  AUTHOR: Leilani Hagen
  DATE: 10/13/2018
 
  ***NOT WORKING - selects duplicate SalesOrderIDs.
  In this stored procedure we are creating a method of verifying the accuracy of the software that
  populated the Sales.SalesOrderHeader and Sales.SalesOrderDetail tables. We are doing so by manually
  computing the subtotal (CalculatedSubTotalFromDetail) for each order (based on SalesOrderID), and
  comparing this calculated subtotal to the original subtotal reported in the SalesOrderHeader table
  (OriginalSubTotal).
  
  REFERENCE MATERIAL:
  -Learning about correlated subqueries:
  https://www.essentialsql.com/get-ready-to-learn-sql-server-20-using-subqueries-in-the-select-statement/

*/

/* RUN THE PROC:

USE AdventureWorks2014

EXEC [dbo].[NotWorking_SalesOrderDataReview]

*/

CREATE OR ALTER PROC [dbo].[NotWorking_SalesOrderDataReview]
AS
	SELECT
		/* Grab the necessary tables created in t1: */
		t1.SalesOrderID,
		t1.SalesOrderNumber,
		t1.OriginalSubTotal,
		t1.CalculatedSubtotalFromDetail,
		/* The difference computed from the OriginalSubtotal and the one calculated
		   as the sum of the LineTotals for each order inside t1: */
		(t1.OriginalSubTotal - t1.CalculatedSubTotalFromDetail) AS Difference
	FROM
		/* Create the OUTER nested select statement. This will select each unique SalesOrderID: */
		(SELECT
			sod_o.SalesOrderID,
			soh.SalesOrderNumber,
			soh.SubTotal AS OriginalSubTotal,
			/* Create the INNER nested select statement, which selects every LineTotal for the
			   current unique SalesOrderID from the OUTER nested select: */
			(SELECT
				SUM(sod_i.LineTotal)
			FROM Sales.SalesOrderDetail sod_i -- SalesOrderDetail_inner
			WHERE sod_i.SalesOrderID = sod_o.SalesOrderID
			) AS CalculatedSubTotalFromDetail
		FROM Sales.SalesOrderDetail sod_o --SalesOrderDetail_outer
			INNER JOIN Sales.SalesOrderHeader soh
				ON sod_o.SalesOrderID = soh.SalesOrderID
		) AS t1
	ORDER BY t1.SalesOrderNumber
	
-- Things I tried...:

/*
		(SELECT
			SUM(sod_i.LineTotal)
		FROM Sales.SalesOrderDetail sod_i -- SalesOrderDetail_inner
		WHERE sod_i.SalesOrderID = sod_o) AS CalculatedSubtotalFromDetail
*/

--More convolouted way of getting SUM(LineTotal):
/*
CREATE OR ALTER PROC [dbo].[SalesOrderDataReview]
AS
	SELECT
		sod_o.SalesOrderID,
		soh.SalesOrderNumber,
		soh.SubTotal AS OriginalSubTotal,
		(SELECT
			SUM(sod_i.UnitPrice*(1 - sod_i.UnitPriceDiscount)*OrderQty) -- Same as "LineTotal"
		FROM Sales.SalesOrderDetail sod_i -- SalesOrderDetail_inner
		WHERE sod_i.SalesOrderID = sod_o.SalesOrderID
		) AS CalculatedSubTotalFromDetail,
		(SubTotal - CalculatedSubTotalFromDetail) AS [Difference]
	FROM Sales.SalesOrderDetail sod_o --SalesOrderDetail_outer
		INNER JOIN Sales.SalesOrderHeader soh
			ON sod_o.SalesOrderID = soh.SalesOrderID

*/

-- Inner SELECT in the FROM statement:
/*
CREATE OR ALTER PROC [dbo].[SalesOrderDataReview]
AS
	SELECT
		sod_o.SalesOrderID,
		soh.SalesOrderNumber,
		soh.Subtotal AS OriginalSubTotal,
		SUM(aipo.LineTotal) AS CalculatedSubTotalFromDetail,
		(soh.Subtotal - SUM(aipo.LineTotal)) AS Difference
	FROM
		Sales.SalesOrderDetail sod_o --SalesOrderDetail_outer
		INNER JOIN Sales.SalesOrderHeader soh
			ON sod_o.SalesOrderID = soh.SalesOrderID,
		(SELECT
		sod_i.LineTotal
		FROM Sales.SalesOrderDetail sod_i -- SalesOrderDetail_inner
		WHERE sod_i.SalesOrderID = sod_o.SalesOrderID
		) AS aipo -- All Items Per Order
 */