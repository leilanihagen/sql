/*
  AUTHOR: Leilani Hagen
  DATE: 11/13/2018

  DESCRIPTION:
  This SQL will demonstrate:
  1) Shredding XML data into table columns while preserving schema (group with the same
  FoodID, etc) from an existing XML document.
  2) Creating XML back from a SQL table using the correct root and parent/child elements,
  3) Displaying data from a SQL table as values in a comma-separated list, using the STUFF() function.

  Run this proc with options to see the different things it does:
  @OUTPUTCODE = 1 to see temp table from original XML,
  @OUTPUTCODE = 2 to see the XML created back from the temp table,
  @OUTPUTCODE = 3 to see the comma-separated list of data.

  REFERENCE MATERIAL:
  -Article teaching the basics of XML manipulation in SQL Server:
  https://www.red-gate.com/simple-talk/sql/database-administration/manipulating-xml-data-in-sql-server/
  
  -Article explaining XML namespaces by w3schools, which was very helpful:
  https://www.w3schools.com/xml/xml_namespaces.asp
  
  -Article explaining differences between using OpenXML() and XQuery methods:
  http://www.sqlservercentral.com/articles/XML/87685/
  
  -How to insert XML into a table:
  https://stackoverflow.com/questions/21378729/how-to-insert-xml-data-into-sql-server-table
  
  -Article explaining how/when to use temp tables:
  https://codingsight.com/introduction-to-temporary-tables-in-sql-server/
  
  -Stack Overflow post showing how to get properly formatted XML out of a SQL table:
  https://stackoverflow.com/questions/1564541/how-to-convert-records-in-a-table-to-xml-format-using-t-sql
  
  -Microsoft doc on STUFF function:
  https://docs.microsoft.com/en-us/sql/t-sql/functions/stuff-transact-sql?view=sql-server-2017
  
  -Stack Overflow post on using STUFF function:
  https://stackoverflow.com/questions/31211506/how-stuff-and-for-xml-path-work-in-sql-server

  QUESTIONS:
  -Why exactly do we need the [1]?
  -Do you need to use T.C. to access the result of @input.nodes, or is C. sufficient?
   
  PROBLEMS:
  -Running this query with [5] instead of [1] returned NULLs... I thought that [5] was needed
  since there were 5 separate occurrences of each value.
   
*/

* /* RUN THE PROC:
   Run with: @OUTPUTCODE = 1 to see temp table from original XML,
             @OUTPUTCODE = 2 to see the XML created back from the temp table,
             @OUTPUTCODE = 3 to see the comma-separated list of data. */

EXEC [dbo].[XMLFun]
@OUTPUTCODE = 3



CREATE OR ALTER PROC [dbo].[XMLFun]
@OUTPUTCODE INT
AS
	/* Store our original XML sample in a variable: */
	DECLARE @rawXML XML =
	'
	<breakfast_menu>
		<food id="1">
			<name>Belgian Waffles</name>
			<price>$5.95</price>
			<description>Two of our famous Belgian Waffles with plenty of real maple syrup</description>
			<calories>650</calories>
		</food>
		<food id="2">
			<name>Strawberry Belgian Waffles</name>
			<price>$7.95</price>
			<description>Light Belgian waffles covered with strawberries and whipped cream</description>
			<calories>900</calories>
		</food>
		<food id="3">
			<name>Berry-Berry Belgian Waffles</name>
			<price>$8.95</price>
			<description>Light Belgian waffles covered with an assortment of fresh berries and whipped cream</description>
			<calories>900</calories>
		</food>
		<food id="4">
			<name>French Toast</name>
			<price>$4.50</price>
			<description>Thick slices made from our homemade sourdough bread</description>
			<calories>600</calories>
		</food>
		<food id="5">
			<name>Homestyle Breakfast</name>
			<price>$6.95</price>
			<description>Two eggs, bacon or sausage, toast, and our ever-popular hash browns</description>
			<calories>950</calories>
		</food>
	</breakfast_menu>
	'
/* Create a temporary table to hold the information stored in our XML @input: */
CREATE TABLE #BreakfastMenu
(
	FoodID INT
	,Name NVARCHAR(100)
	,Price NVARCHAR(50) -- Use an NVARCHAR to preserve the $ marks.
	,Description NVARCHAR(1000)
	,Calories INT
)

/* Populate the temporary table from our XML @input*/
INSERT INTO #BreakfastMenu
	SELECT
		Food.Attributes.value('@id', 'INT') AS FoodID
		,Food.Attributes.value('name[1]', 'NVARCHAR(100)') AS Name
		,Food.Attributes.value('price[1]', 'NVARCHAR(50)') AS Price
		,Food.Attributes.value('description[1]', 'NVARCHAR(1000)') AS Description
		,Food.Attributes.value('calories[1]', 'INT') AS Calories
	FROM @rawXML.nodes('/breakfast_menu/food') AS Food(Attributes)

/* Output the temp table #BreakfastMenu: */
IF (@OUTPUTCODE = 1)
BEGIN
	SELECT *
	FROM #BreakfastMenu
END

/* Convert the data from our temp table #BreakfastMenu back to XML data and output the resulting XML: */
IF (@OUTPUTCODE = 2)
BEGIN
	SELECT
		FoodID AS '@id' -- Specify id as an XML attribute within food tag.
		,Name AS 'name'
		,Price AS 'price'
		,Description AS 'description'
		,Calories AS 'calories'
	FROM #BreakfastMenu
	FOR XML PATH('food'), ROOT('breakfast_menu')
END

/* Select a row from our temp table #BreakfastMenu and output its contents as comma-separated value lists:
	 STEP-BY-STEP: 1) SELECT each Name field from #BreakfastMenu prefixed with a comma and space.
	 							 2) Output this result as XML using FOR XML (leaving an empty PATH so we just get concatenation
	 							    of all the values from our SELECT statement without any XML tags).
	 							 3) Use remaining STUFF() arguments to delete the leading comma at the beginning of the list,
	 							    and finally STUFF everything into an empty string. */
IF (@OUTPUTCODE = 3)
BEGIN
	SELECT
		STUFF( (SELECT ',' + CAST(Name AS NVARCHAR) -- 1)
	      		FROM #BreakfastMenu
			      FOR XML PATH('')) -- 2)
		  			, 1, 1, '') AS FoodIDList -- 3)
END

/* THINGS I TRIED:

-- Table of comma separated lists for each column...:

SELECT TOP 1
		STUFF( (SELECT ', ' + CAST(FoodID AS NVARCHAR)
							FROM #BreakfastMenu
	  							 FOR XML PATH('')), 1, 1, '') AS FoodIDList
		,STUFF( (SELECT ', ' + Name
						 FROM #BreakfastMenu
									FOR XML PATH('')), 1, 1, '') AS NameList
		,STUFF( (SELECT ', ' + Price
						 FROM #BreakfastMenu
									FOR XML PATH('')), 1, 1, '') AS PriceList
		,STUFF( (SELECT ', ' + Description
						 FROM #BreakfastMenu
									FOR XML PATH('')), 1, 1, '') AS DescriptionList
		,STUFF( (SELECT ', ' + CAST(Calories AS NVARCHAR)
						 FROM #BreakfastMenu
									FOR XML PATH('')), 1, 1, '') AS CaloricContentList
FROM #BreakfastMenu


-- Three different ways I tried to shred XML:

-- 1:
SELECT
	B.value('@id', 'INT') AS FoodID
	,B.value('@name[5]', 'VARCHAR(100)') AS Name
	,B.value('@calories[5]', 'INT') AS Calories
	
FROM @input.nodes('/breakfast_menu') AS T(C)
	CROSS APPLY @input.nodes('/breakfast_menu/food') AS A(B)

-- 2:
DECLARE @Doc AS XML
SET @Doc = (SELECT *
			FROM OPENROWSET(BULK '/Users/Leila/Programming/SQL/Interject_Test_Project/XML_Breakfast_Menu.xml', SINGLE_BLOB) as m)
SELECT
	m.value('(/ns:name)[1]', 'nvarchar(50)') AS Name

--3:
;WITH XMLNAMESPACES ('~/Programming/SQL/Interject_Test_Project/XML_Breakfast_Menu.xml' AS ns)
	SELECT

	SELECT
		A.c.value('@id', 'INT') AS FoodID
		,B.d.value('name[5]', 'VARCHAR(100)') AS Name
		,c.value('calories[5]', 'MONEY') AS Price
		
	FROM @input.nodes('/breakfast_menu') AS A(c)
		OUTER APPLY
		A.c.nodes('/food') as B(d)

	;WITH XMLNAMESPACES(DEFAULT 'http://www.webserviceX.NET/')

*/