USE AdventureWorks2014

/*
   AUTHOR: Leilani Hagen
   DATE: 10/10/2018
   
   Select the top 100 records from the columns we need, with some special formatting in a couple of cases.
   
   REFERENCE MATERIAL:
   To translate Status codes into short descriptions:
   - Microsoft doc:
     https://docs.microsoft.com/en-us/sql/t-sql/language-elements/case-transact-sql?view=sql-server-2017
   - Meaning of Status codes:
     http://elsasoft.com/samples/sqlserver_adventureworks/SqlServer.SPRING.KATMAI.AdventureWorks/allTableColumns.htm
*/

SELECT TOP 100
		soh.SalesOrderID,
		soh.RevisionNumber,
		soh.OrderDate,
		/*CASE soh.Status
			WHEN 1 THEN 'In process'
			WHEN 2 THEN 'Approved'
			WHEN 3 THEN 'Backordered'
			WHEN 4 THEN 'Rejected'
			WHEN 5 THEN 'Shipped'
			WHEN 6 THEN 'Cancelled'
		END AS OrderStatusText, */
		dbo.ufnGetSalesOrderStatusText(soh.Status) AS OrderStatusText, -- Cleaner way to get Status descriptions.
		soh.SalesOrderNumber,
		soh.SubTotal,
		soh.TaxAmt,
		soh.Freight,
		soh.TotalDue,
    /* Zero-pad the output, with field width of 4, and print as a string "OrderQtyText":
    	 RIGHT() extracts 4 chars from the string input, starting from the right, where the string being extracted
    	 is '0000' + OrderQty number. */
		RIGHT(CONCAT('0000', sod.OrderQty), 4) AS OrderQtyText,
		sod.UnitPrice,
		sod.UnitPriceDiscount,
		sod.LineTotal,
		a.City AS ShipToCity,
		sp.StateProvinceCode AS ShipToState,
		a.PostalCode AS ShipToZip,
		cr.CountryRegionCode AS ShipToCountryCode,
		cr.Name AS ShipToCountryName,
		pm.Name AS ProductModel,
		pc.Name AS CategoryName
	FROM Sales.SalesOrderHeader soh
	/* Join the two most fundamental tables, soh and sod: */
		INNER JOIN Sales.SalesOrderDetail sod
			ON soh.SalesOrderID = sod.SalesOrderID
	/* Join in necessary tables for shipping/location related records: */
		INNER JOIN Person.Address a
			ON soh.ShipToAddressID = a.AddressID
		INNER JOIN Person.StateProvince sp
			ON a.StateProvinceID = sp.StateProvinceID
		INNER JOIN Person.CountryRegion cr
			ON sp.CountryRegionCode = cr.CountryRegionCode
	/* Join in tables necessary for ProductModel record: */
		INNER JOIN Production.Product p
			ON sod.ProductID = p.ProductID
		INNER JOIN Production.ProductModel pm
			ON p.ProductModelID = pm.ProductModelID
	/* Join in tables necessary for CategoryName record: */
		INNER JOIN Production.ProductSubcategory ps
			ON p.ProductSubcategoryID = ps.ProductSubcategoryID
		INNER JOIN Production.ProductCategory pc
			ON ps.ProductCategoryID = pc.ProductCategoryID
	ORDER BY soh.SalesOrderNumber;
/*
	(SELECT
		a.City,
		ROW_NUMBER() OVER (ORDER BY a.City) rn
	FROM
		Person.Address a,
		Sales.SalesOrderHeader soh
	WHERE
		soh.BillToAddressID = a.AddressID
	) as t2
ON t1.rn = t2.rn;