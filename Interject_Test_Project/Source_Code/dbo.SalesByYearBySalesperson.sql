/*
  AUTHOR: Leilani Hagen
  DATE: 12/10/2018

  In this SQL I am demonstrating how to use the PIVOT operator to aggregate total sales quota by salesperson
  by year. I am also providing comparison by displaying this information with and without the use of PIVOT to
  compare readability of both.

  Run query with:
      @OUTPUTCODE = 1 to see result of query WITHOUT PIVOT
      @OUTPUTCODE = 2 to see result of query WITH PIVOT

  REFERENCE MATERIAL:
  -Video explaining why you might use pivot/unpivot, and how to use them:
  https://www.youtube.com/watch?v=h3BtudZehuo
  -Microsoft doc explaining pivot/unpivot:
  https://www.c-sharpcorner.com/UploadFile/f0b2ed/pivot-and-unpovit-in-sql-server/
   
*/

/* /* RUN THE PROC:
   Run with: @OUTPUTCODE = 1 to see result of query WITHOUT PIVOT
             @OUTPUTCODE = 2 to see result of query WITH PIVOT */

USE AdventureWorks2014

EXEC [dbo].[SalesByYearBySalesperson]
@OUTPUTCODE = 2

*/

CREATE OR ALTER PROC [dbo].[SalesByYearBySalesperson]
@OUTPUTCODE INT
AS

  /* What does the data look like without a pivot? */
  IF (@OUTPUTCODE = 1)
  BEGIN
    SELECT
      CONCAT(p.FirstName, ' ', p.MiddleName, ' ', p.LastName) AS SalesPerson
      ,YEAR(h.QuotaDate) AS Year
      ,h.SalesQuota
    FROM Sales.SalesPersonQuotaHistory h
      INNER JOIN Person.Person p
       ON p.BusinessEntityID = h.BusinessEntityID
  END

  /* With pivot: */
  IF (@OUTPUTCODE = 2)
  BEGIN
    SELECT
       CONCAT(p.FirstName, ' ', p.MiddleName, ' ', p.LastName) AS SalesPerson
       /* Each year as our new (pivoted) columns: */
      ,[2011]
      ,[2012]
      ,[2013]
      ,[2014]
    FROM
      /* Using nested SELECT b/c PIVOT() does not want YEAR() function inside of it: */
      (SELECT
        BusinessEntityID
        ,YEAR(QuotaDate) AS Year
        ,SalesQuota AS SalesQuota
      FROM
        Sales.SalesPersonQuotaHistory) AS PivotCols
    /* Pivot total sales (aggregate) for each employee around Year: */
    PIVOT
    (
      SUM(SalesQuota)
      FOR Year
      IN ([2011], [2012], [2013], [2014])
    ) AS qh -- "quota history"
    /* Join pivoted data to Person to get the Salespersons names: */
    INNER JOIN Person.Person p
      ON p.BusinessEntityID = qh.BusinessEntityID
  END