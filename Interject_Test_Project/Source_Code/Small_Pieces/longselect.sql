SELECT TOP 100
	soh.SalesOrderID,
	soh.RevisionNumber,
	soh.OrderDate,
	CASE soh.Status
		WHEN 1 THEN 'In process'
		WHEN 2 THEN 'Approved'
		WHEN 3 THEN 'Backordered'
		WHEN 4 THEN 'Rejected'
		WHEN 5 THEN 'Shipped'
		WHEN 6 THEN 'Cancelled'
	END AS OrderStatusText,
	soh.SalesOrderNumber,
	soh.SubTotal,
	soh.TaxAmt,
	soh.Freight,
	soh.TotalDue,
	RIGHT(CONCAT('0000', sod.OrderQty), 4) AS OrderQtyText,
	sod.UnitPrice,
	sod.UnitPriceDiscount,
	sod.LineTotal,
	(SELECT a.City
	FROM Person.Address a, Sales.SalesOrderHeader soh
	WHERE )
FROM
	Sales.SalesOrderHeader soh,
	Sales.SalesOrderDetail sod,
	Person.Address
FULL OUTER JOIN
	(SELECT
		a.City,
	FROM
		Person.Address a,
		Sales.SalesOrderHeader soh
	WHERE
		soh.BillToAddressID = a.AddressID
	) as t2
ON t1.rn = t2.rn;