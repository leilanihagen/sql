USE AdventureWorks2014

SELECT City as ShipToCity
FROM Person.Address, Sales.SalesOrderHeader
WHERE Person.Address.AddressID = Sales.SalesOrderHeader.BillToAddressID;