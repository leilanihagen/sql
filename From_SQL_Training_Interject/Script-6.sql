SELECT o.OrderID, o.CustomerID, o.EmployeeID, o.OrderDate, od.*
FROM Northwind.dbo.Orders o
Inner JOIN Northwind.dbo.[Order Details] od
ON od.OrderID = o.OrderID
WHERE EmployeeID =  4

SELECT LastName, FirstName, ISNULL(t.TotalAmount, 0) AS TotalAmount
FROM Northwind.dbo.Employees
LEFT JOIN (

SELECT o.EmployeeID, SUM(UnitPrice * Quantity) AS TotalAmount
FROM Northwind.dbo.[Orders]
INNER JOIN Northwind.dbo.[Order Details]
ON od.OrderID = o.OrderID
) t
ON t.EmployeeID - e.EmployeeID;