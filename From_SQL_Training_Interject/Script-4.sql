DECLARE @CompanyName nvarchar(40) = 'mark'

SELECT CustomerID, CompanyName, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone, Fax, ClientID
FROM Northwind.dbo.Customers
WHERE CompanyName LIKE '%' + @CompanyName + '%'
ORDER BY CompanyName DESC

SELECT Country, COUNT(*) AS CountOfCountries
FROM Northwind.dbo.Customers
GROUP BY Country;