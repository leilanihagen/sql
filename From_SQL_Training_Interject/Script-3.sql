ALTER PROC dbo.MyFirstStoredProcedure -- Like a function

@CompanyName nvarchar(40) = 'mark'

AS

-- Test procedure:
/*
EXEC dbo.MyFirstStoredProcedure
@CompanyName = 'some company name'
*/

SELECT CustomerID, CompanyName, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone, Fax, ClientID
FROM Northwind.dbo.Customers
WHERE CompanyName LIKE '%' + @CompanyName + '%'
ORDER BY CompanyName DESC;

