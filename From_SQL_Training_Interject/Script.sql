create DATABASE Northwind;

GO


create login test with password = 'test',
CHECK_EXPIRATION = off, CHECK_POLICY = off;

alter login test enable;